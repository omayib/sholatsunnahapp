package lostmind.kreatip.shalatsunnah;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.agimind.widget.SlideHolder;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class ShalatSunnah extends Activity {

	private SlideHolder mSlideHolder;
	View toggleView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shalat_sunnah);
		mSlideHolder = (SlideHolder) findViewById(R.id.slideHolder);

		Toast.makeText(
				this,
				"Silahkan pilih menu dengan slide ke kanan atau tekan icon pojok kiri atas.",
				Toast.LENGTH_LONG).show();
		toggleView = findViewById(R.id.webView1);
		WebView wv;
		wv = (WebView) findViewById(R.id.webView1);
		wv.loadUrl("file:///android_asset/www/dhuha.html");

		// Create the adView
		AdView adView = new AdView(this, AdSize.SMART_BANNER, "a1518f9e1a8303d");
		// Lookup your LinearLayout assuming it's been given
		// the attribute android:id="@+id/mainLayout"
		LinearLayout ll = (LinearLayout) findViewById(R.id.iklan);
		// Add the adView to it
		ll.addView(adView);
		// Initiate a generic request to load it with an ad
		adView.loadAd(new AdRequest());

	}

	public void onActionButton(View v) {
		mSlideHolder.toggle();
	}

	public void Dhuha(View v) {

		toggleView = findViewById(R.id.webView1);
		WebView wv;
		wv = (WebView) findViewById(R.id.webView1);
		wv.loadUrl("file:///android_asset/www/dhuha.html");

	}

	public void Istikharah(View v) {

		toggleView = findViewById(R.id.webView1);
		WebView wv;
		wv = (WebView) findViewById(R.id.webView1);
		wv.loadUrl("file:///android_asset/www/istikharah.html");

	}

	public void Rawatib(View v) {

		toggleView = findViewById(R.id.webView1);
		WebView wv;
		wv = (WebView) findViewById(R.id.webView1);
		wv.loadUrl("file:///android_asset/www/rawatib.html");

	}

	public void Tahajud(View v) {

		toggleView = findViewById(R.id.webView1);
		WebView wv;
		wv = (WebView) findViewById(R.id.webView1);
		wv.loadUrl("file:///android_asset/www/tahajud.html");

	}

	public void Taubat(View v) {

		toggleView = findViewById(R.id.webView1);
		WebView wv;
		wv = (WebView) findViewById(R.id.webView1);
		wv.loadUrl("file:///android_asset/www/taubat.html");

	}

	public void Hajat(View v) {

		toggleView = findViewById(R.id.webView1);
		WebView wv;
		wv = (WebView) findViewById(R.id.webView1);
		wv.loadUrl("file:///android_asset/www/hajat.html");

	}

	public void rating(View arg0) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		// Try Google play
		intent.setData(Uri
				.parse("market://details?id=lostmind.kreatip.shalatsunnah"));
		startActivity(intent);

	}

	public void applain(View arg0) {
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setData(Uri.parse("market://search?q=Heri+Kiswanto"));
		startActivity(intent);

	}

	// override method onKeyUp
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode != KeyEvent.KEYCODE_BACK)
			return super.onKeyDown(keyCode, event);

		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:
					// Yes button clicked
					finish();
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					// No button clicked
					break;
				}
			}
		};

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Apakah anda ingin keluar aplikasi?")
				.setPositiveButton("Ya", dialogClickListener)
				.setNegativeButton("Tidak", dialogClickListener).show();

		return super.onKeyDown(keyCode, event);
	}

	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_MENU) {
			// do your work
			mSlideHolder.toggle();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		return true;
	}
}
