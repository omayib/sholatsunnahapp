package lostmind.kreatip.shalatsunnah;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.Toast;

public class Splash extends Activity {
	protected boolean _active = true;
	protected int _splashTime = 1500;
	private AlertDialog.Builder alertDialogBuilder;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.splash);

		// thread for displaying the SplashScreen
		Thread splashTread = new Thread() {
			@Override
			public void run() {
				try {
					int waited = 0;
					while (_active && (waited < _splashTime)) {
						sleep(300);
						if (_active) {
							waited += 100;
						}
					}
				} catch (InterruptedException e) {
					// do nothing
				} finally {
					finish();
					Intent newIntent = new Intent(Splash.this, ShalatSunnah.class);
					startActivityForResult(newIntent, 0);
				}
			}
		};

		if (isNetworkAvailable(getApplicationContext())) {

			Toast.makeText(this,
					"Anda terhubung dengan internet",
					Toast.LENGTH_SHORT).show();
			splashTread.start();

		} else {
			if (alertDialogBuilder == null) {
				alertDialogBuilder = new AlertDialog.Builder(this);
			}

			alertDialogBuilder
					.setMessage("Anda tidak terhubung dengan internet, Keluar?")
					.setPositiveButton("Ya",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									// menghancurkan activity / keluar aplikasi
									finish();
								}
							})

					.create().show();
		}

	}

	public static boolean isNetworkAvailable(Context ctx) {
		ConnectivityManager connMgr = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected()
				|| connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
						.isConnected()) {
			return true;
		}

		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			_active = false;
		}
		return true;
	}
}